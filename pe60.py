#PE 60
import sympy
from itertools import permutations 
def gen_Primes() :
  return [ x for x in range(1000) if sympy.isprime(x)]

def set_of_prime_permutations():
  for i in permutations(gen_Primes(),5):
    print(i)

set_of_prime_permutations()
    
