import itertools as it
def convert_parenthsis(s):
  if s == '(':
    return 1
  elif s == ')':
    return -1
  else :
    return 0
def convert(s):
  return [convert_parenthsis(ch) for ch in s]
def check_paren(s):
  sums = list(it.accumulate(convert(s)))
  return -1 not in sums and sums[-1] == 0


print(list(it.accumulate(convert(")))((("))))
print(check_paren(")))((("))
print(check_paren("((("))
