def longerNumerator(numer , denom):
  return len(str(numer)) > len(str(denom))

def continued_fraction() :
  numer , denom , numer_surplus = 1,  1 , 0
  for i in range(1,1001):
    numer , denom = numer + 2 * (denom) , numer + denom
    if   longerNumerator(numer , denom) :
        numer_surplus += 1
  return numer_surplus
  
continued_fraction()
