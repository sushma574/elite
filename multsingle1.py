from functools import reduce
num = 16795831
def sumTilDigit(num , c):
  if num < 10:
    return c
  else:
    num = sum([int(x) for x in str(num)])
    return sumTilDigit(num , c+ 1)

def ProductTilDigit(num , c):
  if num < 10:
    return c
  else:
    num = reduce((lambda x , y : x * y),([int(x) for x in str(num)]))
    return ProductTilDigit(num , c+ 1)

print(ProductTilDigit(num , 0))
print(sumTilDigit(num , 0))
