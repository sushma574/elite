def calculate_score(cards):
  golf_dict = {"triple bogey" : 3 , "double bogey" : 2 , "bogey" : 1 , "par" : 0 , "birdie" : -1 , "eagle" : -2 , "albatross" : -3 }
  n = sum([golf_dict[x] for x in cards])
  if n == 0 :
    return "you Scored par"
  elif n < 0:
    return "you Scored {} under par".format(abs(n))
  else:
    return "you Scored {} over par"


def golf():
  
  cards = ["birdie" , "eagle" , "birdie" , "birdie" , "birdie"]
  pars = [4, 5, 3, 4, 4]
  print(calculate_score(cards))

golf()
