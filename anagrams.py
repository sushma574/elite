from itertools import groupby
def group_anagrams(words):
  return [list(group) for key,group in groupby(sorted(words,key = sorted), sorted)]
group_anagrams(["as", "Sa" , "ate" , "eat"])
