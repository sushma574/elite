fn main() {
    let  num:i32 = 50;
    fizz_buzz(num);
}

fn fizz_buzz(num : i32) {
    let mut v:Vec<String> = Vec::new();
    for i in 1..num + 1 {
        if i % 15 == 0 {
            v.push("FizzBuzz".to_string());
        }
        else if i % 5 == 0 {
            v.push("Buzz".to_string());
        }
        else if i % 3 == 0 {
            v.push("Fizz".to_string());
        }
        else {
            v.push(i.to_string());
        }
    }
    println!("{:?}",v);
}
