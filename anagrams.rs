use std::collections::HashSet;

fn main() {
    let mut v:Vec<String> = Vec::new();
    v.push("listen".to_string());
    v.push("silent".to_string());
    v.push("eat".to_string());
    v.push("ate".to_string());
    v.push("tea".to_string());
    v.push("cow".to_string());
    v.push("ect".to_string());
    v.push("strap".to_string());
    v.push("traps".to_string());
    anagram(v);
}

fn anagram(mut v : Vec<String>) {
    let mut res :HashSet<String> = HashSet::new();
    for i in 0..v.len() {
        for j in i + 1..v.len() {
            if is_anagram(v[i].to_string() , v[j].to_string()) == true {
                res.insert(v[j].to_string());
                res.insert(v[i].to_string());
            }
        }
    }
    println!("{:?}",res);
}

fn is_anagram(s1 :String , s2 : String) -> bool {
    if s1.len() == s2.len() {
        let mut p : HashSet<char> = HashSet::new();
        for x in s1.chars() {
            p.insert(x);
        }
        for  y in s2.chars() {
            p.insert(y);
        }
        if p.len() == s1.len()  {
           return true;
        }
     }
    return false;
}
